# -*- coding: utf-8 -*-

from setuptools import setup, find_namespace_packages

readme = ''

setup(
    long_description=readme,
    name='sensoriplexer',
    version='0.1.0',
    python_requires='==3.*,>=3.7.0',
    author='Eric Platon',
    author_email='zaraki@gmx.com',
    packages=find_namespace_packages(
        where='src',
        exclude=[]
    ),
    package_data={
        '': [ '*.hdf5' ],
    },
    install_requires=[
        'numpy==1.*,>=1.18.1',
        'torch==1.*,>=1.4.0',
    ],
    extras_require={
        'dev': [
            # For the evaluation/demo scripts.
            'opencv-contrib-python==4.*,>=4.2.0',
            'av==6.*,>=6.2.0',
            'matplotlib==3.*,>=3.2.1',
            'torchvision==0.*,>=0.5.0',
        ]
    },
)
